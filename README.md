# APP - INDICADORES

Este proyecto contiene código fuente de una aplicación Node.js utilizando Express.


## Requisitos e Instalación 🔧


- Node.js - [Install Node.js 10](https://nodejs.org/en/), including the NPM package management tool.
- [Git](https://www.atlassian.com/git/tutorials/install-git)


Ahora que tienes lo necesario , necesitas hacer el clon del proyecto :

```
https://gitlab.com/diegoescaff/back-app-indicadores.git
```

lo primero es ingresar a la carpeta `/back-app-indicadores` y ejecutar el siguiente comando que instala las dependencias definidas en el archivo `package.json`

```
npm install
```

La aplicación viene previamente configurada con credenciales para realizar pruebas de inmediato, pero puedes cambiarlas por tus propias credenciales en el archivo `.env`.

> **OJO 👀:** Si utilizas un puerto de tu preferencia recuerda cambiarlo, solo en ese campo


```bash
    PORT=3000
```


## Despliegue 📦

```bash
npm start
```

Ahora dirígete en tu navegador a `http://localhost:3000/api`

### Desarrollo

```bash
npm run dev
```

Ahora dirígete en tu navegador a `http://localhost:3000/api`.

- http://localhost:3000/api-docs [get] - Encontraras la documentación de la APP
- http://localhost:3000/api/home [get] - Obtiene una URL de prueba
- http://localhost:3000/api/indicadores [get]- Obtiene todos los indicadores

## Ejecutando las pruebas ⚙️
```bash
npm run test
```
